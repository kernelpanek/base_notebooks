JUPYTER_CONFIG_DIR := "${PWD}/etc/jupyter"
JUPYTER_RUNTIME_DIR := "${PWD}/.runtime"

run:
	JUPYTER_CONFIG_DIR=${JUPYTER_CONFIG_DIR} JUPYTER_RUNTIME_DIR=${JUPYTER_RUNTIME_DIR} venv/bin/jupyter lab

install: install_jupyter install_kernels install_labextensions install_nbextensions run

install_kernels:
	venv/bin/jupyter kernelspec install kernels/clojure --sys-prefix
	venv/bin/jupyter kernelspec install kernels/gophernotes --sys-prefix
	venv/bin/jupyter kernelspec install kernels/groovy --sys-prefix
	venv/bin/jupyter kernelspec install kernels/java9 --sys-prefix
	venv/bin/jupyter kernelspec install kernels/javascript --sys-prefix
	venv/bin/jupyter kernelspec install kernels/julia-0.6 --sys-prefix
	venv/bin/jupyter kernelspec install kernels/kotlin --sys-prefix
	venv/bin/jupyter kernelspec install kernels/python2 --sys-prefix
	venv/bin/jupyter kernelspec install kernels/python37 --sys-prefix
	venv/bin/jupyter kernelspec install kernels/scala --sys-prefix
install_nbextensions:
	JUPYTER_CONFIG_DIR=${JUPYTER_CONFIG_DIR} JUPYTER_RUNTIME_DIR=${JUPYTER_RUNTIME_DIR} venv/bin/jupyter contrib nbextension install --sys-prefix
	JUPYTER_CONFIG_DIR=${JUPYTER_CONFIG_DIR} JUPYTER_RUNTIME_DIR=${JUPYTER_RUNTIME_DIR} venv/bin/jupyter cms quick-setup --sys-prefix
	JUPYTER_CONFIG_DIR=${JUPYTER_CONFIG_DIR} JUPYTER_RUNTIME_DIR=${JUPYTER_RUNTIME_DIR} venv/bin/jupyter serverextension enable --py lantern
	JUPYTER_CONFIG_DIR=${JUPYTER_CONFIG_DIR} JUPYTER_RUNTIME_DIR=${JUPYTER_RUNTIME_DIR} venv/bin/jupyter serverextension enable --py jupyter_cms --sys-prefix
	JUPYTER_CONFIG_DIR=${JUPYTER_CONFIG_DIR} JUPYTER_RUNTIME_DIR=${JUPYTER_RUNTIME_DIR} venv/bin/jupyter nbextension install --py jupyter_cms --sys-prefix
	JUPYTER_CONFIG_DIR=${JUPYTER_CONFIG_DIR} JUPYTER_RUNTIME_DIR=${JUPYTER_RUNTIME_DIR} venv/bin/jupyter nbextension enable --py jupyter_cms --sys-prefix
	JUPYTER_CONFIG_DIR=${JUPYTER_CONFIG_DIR} JUPYTER_RUNTIME_DIR=${JUPYTER_RUNTIME_DIR} venv/bin/jupyter nbextension install --py --symlink --sys-prefix pythreejs
	JUPYTER_CONFIG_DIR=${JUPYTER_CONFIG_DIR} JUPYTER_RUNTIME_DIR=${JUPYTER_RUNTIME_DIR} venv/bin/jupyter nbextension enable --py --sys-prefix pythreejs

install_labextensions:
	JUPYTER_CONFIG_DIR=${JUPYTER_CONFIG_DIR} JUPYTER_RUNTIME_DIR=${JUPYTER_RUNTIME_DIR} venv/bin/jupyter labextension install @jupyter-widgets/jupyterlab-manager
	JUPYTER_CONFIG_DIR=${JUPYTER_CONFIG_DIR} JUPYTER_RUNTIME_DIR=${JUPYTER_RUNTIME_DIR} venv/bin/jupyter labextension install pylantern
	JUPYTER_CONFIG_DIR=${JUPYTER_CONFIG_DIR} JUPYTER_RUNTIME_DIR=${JUPYTER_RUNTIME_DIR} venv/bin/jupyter labextension install jupyterlab_bokeh
	JUPYTER_CONFIG_DIR=${JUPYTER_CONFIG_DIR} JUPYTER_RUNTIME_DIR=${JUPYTER_RUNTIME_DIR} venv/bin/jupyter lab build


install_jupyter:
	python -m venv --copies venv
	venv/bin/pip install -U pip
	venv/bin/pip install -r requirements.txt
	mkdir -p .runtime

clean_venv:
	rm -rf venv
