## Notebooks

To start the lab, have Python 3 preinstalled on your machine, then run:

```bash
make install
```
