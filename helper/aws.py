import boto3
import pathlib
import configparser
from collections import namedtuple
import botocore.exceptions
import botocore.model
import configparser
import io, os, pwd
from itertools import repeat
import asyncio
import sys
import logging
import logging.config
import json
import html
import json
from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import Terminal256Formatter
from pprint import pformat
from subprocess import run

from IPython.core.display import display, HTML
from jinja2 import Template, FileSystemLoader, Environment


def update_okta_profile(profile):
  cmd = f"oaws --profile {profile}"
  run(cmd, shell=True)

AWS_US_REGIONS = ["us-east-1", "us-east-2", "us-west-1", "us-west-2"]
AWS_CA_REGIONS = ["ca-central-1"]
AWS_SA_REGIONS = ["sa-east-1"]
AWS_EU_REGIONS = ["eu-west-3", "eu-west-2", "eu-west-1", "eu-central-1"]
AWS_ASIA_REGIONS = ["ap-south-1", "ap-northeast-2", "ap-northeast-1", "ap-southeast-1", "ap-southeast-2"]
AWS_ALL_REGIONS = AWS_US_REGIONS + AWS_CA_REGIONS + AWS_SA_REGIONS + AWS_EU_REGIONS + AWS_ASIA_REGIONS

AWS_SERVICES_CONDENSED = ["cloudfront", "cloudtrail", "ec2", "s3", "elb", "iam", "rds", "route53", "route53domains", "sns", "sqs", "sts"]
AWS_SERVICES_DATA = ["athena", "rds", "dynamodb", "elasticache", "redshift", "neptune", "dms"]
AWS_SERVICES_COMPUTE = ["ec2", "lambda", "stepfunctions"]
AWS_SERVICES_OPS = ["cloudformation", "opsworks", "opsworkscm", "ssm"]
AWS_SERVICES_MGMT = ["cloudtrail", "cloudwatch", "budgets", "config", "cur", "events", "iam", "logs", "organizations", "pricing", "servicecatalog", "ssm", "sts"]

logging_config = json.loads("""
{
    "version": 1,
    "disable_existing_loggers": true,
    "formatters": {
        "simple": {
            "format": "%(asctime)s | %(levelname)s | (%(funcName)s:%(lineno)d) | %(message)s"
        }
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "simple",
            "stream": "ext://sys.stdout"
        }
    },
    "root": {
        "level": "INFO",
        "handlers": ["console"]
    }
}
""")

logging.config.dictConfig(logging_config)
logger = logging.getLogger()

if sys.version_info[0] == 3 and sys.version_info[1] > 5:
    import asyncio.events as _ae
    import os as _os

    _ae._RunningLoop._pid = None

    def _get_running_loop():
        if _ae._running_loop._pid == _os.getpid():
            return _ae._running_loop._loop

    def _set_running_loop(loop):
        _ae._running_loop._pid = _os.getpid()
        _ae._running_loop._loop = loop

    _ae._get_running_loop = _get_running_loop
    _ae._set_running_loop = _set_running_loop

NamedConnection = namedtuple("NamedConnection", "profile connection region service")

def list_profile_names(profile_exclusions=[]):
    config = configparser.ConfigParser()
    config.read(pathlib.Path("~/", ".aws", "config").expanduser())
    profile_names = [section.replace("profile ", "") for section in config.sections()]
    exclude = profile_exclusions
    profile_list = [x for x in profile_names if x not in exclude]
    return(profile_list)


async def get_session_client(session, service):
    session_client = session.client(service)
    logger.debug("Session Client connected to {}".format(service))
    return service, session_client


async def make_all_connections(profiles=[], regions=[], svc_include=[], svc_exclude=[], loop=None):
    account_connections = {}
    for profile in profiles:
        update_okta_profile(profile)
        account_connections.update({profile: {}})
        for region in regions:
            sess = boto3.session.Session(profile_name=profile, region_name=region)
            account_connections[profile].update({region:{}})
            connections = {}
            services = sess.get_available_services()
            if len(svc_include) > 0:
                services = [svc for svc in services if svc in svc_include]
            elif len(svc_exclude) > 0:
                services = [svc for svc in services if svc not in svc_exclude]
            deferred_sessions = [get_session_client(sess, service) for service in services]
            gathered_sessions = await asyncio.gather(*deferred_sessions, return_exceptions=True, loop=loop)
            for service, session_client in gathered_sessions:
                connections[service] = session_client
                logger.info("Service Connection Acquired: {0}.{1}.{2}".format(profile, region, service))

            account_connections[profile][region] = connections
    return(account_connections)

async def make_all_connections2(profiles=[], regions=[], svc_include=[], svc_exclude=[], loop=None):
    account_connections = []
    for profile in profiles:
        for region in regions:
            sess = boto3.session.Session(profile_name=profile, region_name=region)
            services = sess.get_available_services()
            if len(svc_include) > 0:
                services = [svc for svc in services if svc in svc_include]
            elif len(svc_exclude) > 0:
                services = [svc for svc in services if svc not in svc_exclude]
            deferred_sessions = [get_session_client(sess, service) for service in services]
            gathered_sessions = await asyncio.gather(*deferred_sessions, return_exceptions=True, loop=loop)
            for service, session_client in gathered_sessions:
                named_conn = NamedConnection(profile=profile, region=region, service=service, connection=session_client)
                account_connections.append(named_conn)
                logger.info("Service Connection Acquired: {0}.{1}.{2}".format(profile, region, service))
            logger.info("{}.{} connected.".format(profile, region))
    return(account_connections)

def list_regions():
    sess = boto3.session.Session(profile_name=list_profile_names()[0])
    ec2 = sess.client('ec2')
    regions = ec2.describe_regions().get('Regions')
    return([region.get('RegionName') for region in regions])


def list_services(region):
    sess = boto3.session.Session(profile_name=list_profile_names()[0], region_name=region)
    return(sess.get_available_services())


def aws_connect(profiles=[], regions=[], svc_include=[], svc_exclude=[]):
    loop = asyncio.new_event_loop()
    aws_conn = loop.run_until_complete(make_all_connections(profiles=profiles, regions=regions, svc_include=svc_include, svc_exclude=svc_exclude, loop=loop))
    return aws_conn

def aws_connect2(profiles=[], regions=[], svc_include=[], svc_exclude=[]):
    loop = asyncio.new_event_loop()
    aws_conn = loop.run_until_complete(make_all_connections2(profiles=profiles, regions=regions, svc_include=svc_include, svc_exclude=svc_exclude, loop=loop))
    return aws_conn


def pprint_color(obj):
    print(highlight(pformat(obj), PythonLexer(), Terminal256Formatter()))


def render_template(template_file, **kwargs):
    templateLoader = FileSystemLoader(searchpath="./")
    templateEnv = Environment(loader=templateLoader)

    template = templateEnv.get_template(template_file)
    outputText = template.render(**kwargs)
    return outputText


def get_shape_data(client, shape_for):
    shape = client.meta.service_model.shape_for(shape_for)
    shape_return = {botocore.model.StringShape: lambda x: dict(enum=x.enum, docs=x.documentation),
                    botocore.model.StructureShape: lambda x: dict(name=x.name, required=x.required_members, members={k: get_shape_data(client, v.name) for k, v in x.members.items()}, docs=x.documentation),
                    botocore.model.ListShape: lambda x: get_shape_data(client, x.member.name),
                    botocore.model.MapShape: lambda x: dict(type=str(type(x)), name=x.name),
                    botocore.model.Shape: lambda x: dict(type=x.name)}
    return shape_return.get(type(shape), lambda x: dict())(shape)


def generate_html_for(method, param_name=None):
    page_src = ""
    client = method.__self__
    method_name = client.meta.method_to_api_mapping[method.__func__.__name__]
    if param_name is None:
        for key, val in client.meta.service_model.operation_model(
                method_name).input_shape.members.items():
            docs = client.meta.service_model.operation_model(
                method_name).input_shape.members[key].documentation
            page_src += "<h3>{0}</h3><h4>{1}</h4>".format(
                key, html.escape(str(val)))
            page_src += "<div>"
            if len(docs) > 0:
                page_src += docs
            page_src += "<pre>{}</pre>".format(
                json.dumps(
                    get_shape_data(client, val.name), indent=2, sort_keys=True))
            page_src += "<div>"
    else:
        param = client.meta.service_model.operation_model(
            method_name).input_shape.members[param_name]
        docs = param.documentation
        page_src += "<h3>{0}</h3><h4>{1}</h4>".format(param_name,
                                                      html.escape(str(param)))
        page_src += "<div>"
        if len(docs) > 0:
            page_src += docs
        page_src += "<pre>{}</pre>".format(
            json.dumps(
                get_shape_data(client, param.name), indent=2, sort_keys=True))
        page_src += "<div>"
    return HTML(page_src)
